package fer.icr.battleship;

import fer.icr.battleship.game.Game;
import fer.icr.battleship.game.IGameContext;
import fer.icr.battleship.game.Player;
import fer.icr.battleship.game.Game.FinishStatus;
import fer.icr.battleship.game.Player.Type;
import android.os.Bundle;
import android.os.Vibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.widget.TextView;

public class GameActivity extends Activity implements IGameContext {

	private TextView lbl1, lbl2;
	private GridSurfaceView surface1, surface2;
	private Player player1, player2;
	private Game game;
	private Vibrator vibrator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		
		lbl1 = (TextView) findViewById(R.id.lblPlayerOne);
		lbl2 = (TextView) findViewById(R.id.lblPlayerTwo);
		surface1 = (GridSurfaceView) findViewById(R.id.surfaceOne);
		surface2 = (GridSurfaceView) findViewById(R.id.surfaceTwo);

		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		startGame();
	}

	public void startGame() {
		
		Player.Type player2type = Type.HUMAN;
		
		if (getIntent().getBooleanExtra("typeVsComputer",false)) {
			player2type = Type.COMPUTER;
		}
		
		player1 = new Player(GameData.instance().player1, Player.Type.HUMAN);
		player2 = new Player(GameData.instance().player2, player2type);

		if (!player1.placeShipsOnTheGrid() || !player2.placeShipsOnTheGrid()) {
			showMessageDialog("ERROR");
			finish();
		}

		game = new Game(this, player1, player2);

		surface1.init(game, player1.getGrid());
		surface2.init(game, player2.getGrid());

		game.start();
	}

	public void showMessageDialog(String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msg);
		builder.create().show();
	}

	@Override
	public void onBackPressed() {

		if (GameData.instance().closeConfirm) {
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Are you sure?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finishGame();
								}

							}).setNegativeButton("No", null).show();
		} else
			finishGame();
	}

	private void finishGame() {
		game.finish();
	}

	@Override
	protected void onResume() {
		surface1.startMainThread();
		surface2.startMainThread();
		super.onResume();
	}

	@Override
	protected void onPause() {
		surface1.stopMainThread();
		surface2.stopMainThread();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	public void updateLabels(Player activePlayer, Player inactivePlayer) {
		SpannableString p1lbl = new SpannableString(player1.getLabel());
		SpannableString p2lbl = new SpannableString(player2.getLabel());

		if (activePlayer == player1) {
			p1lbl.setSpan(new UnderlineSpan(), 0, p1lbl.length(), 0);
			p1lbl.setSpan(new StyleSpan(Typeface.BOLD), 0, p1lbl.length(), 0);
		} else {
			p2lbl.setSpan(new UnderlineSpan(), 0, p2lbl.length(), 0);
			p2lbl.setSpan(new StyleSpan(Typeface.BOLD), 0, p2lbl.length(), 0);
		}

		lbl1.setText(p1lbl);
		lbl2.setText(p2lbl);
	}

	@Override
	public void showPlayerSwitchDialog(Player nextPlayer) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(nextPlayer.getName() + "'s turn!");
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				game.continueAfterSwitchDialog();
			}
		});
		builder.create().show();
	}

	public void showPlayerWinDialog(Player winPlayer) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(winPlayer.getName() + " has win the game!");
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				finish();
			}
		});
		builder.create().show();
	}

	@Override
	public void onMoveMiss() {
	}

	@Override
	public void onMoveInvalid() {
	}

	@Override
	public void onMoveShipHit() {
		if (GameData.instance().vibration)
			vibrator.vibrate(100);
	}

	@Override
	public void onMoveShipSink() {
		if (GameData.instance().vibration)
			vibrator.vibrate(500);
	}

	@Override
	public void onFinish(FinishStatus status) {

		GameData data = GameData.instance();

		if (status == FinishStatus.CANCEL)
			data.canceledGames++;
		else {
			data.totalScore += player1.getScore();
			data.totalGames++;

			if (status == FinishStatus.WIN)
				data.wins++;
			else if (status == FinishStatus.LOSE)
				data.loses++;

			data.avgScore = data.totalScore / data.totalGames;					
		}

		data.saveChanges();
		
		if (status == FinishStatus.WIN || status == FinishStatus.LOSE)
			showPlayerWinDialog(status == FinishStatus.WIN ? player1 : player2);
		else
			finish();
	}
	
	@Override
	public void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
