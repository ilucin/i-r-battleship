package fer.icr.battleship.game;

import fer.icr.battleship.game.Game.FinishStatus;

public interface IGameContext {

	void updateLabels(Player activePlayer, Player inactivePlayer);
	void showPlayerSwitchDialog(Player nextPlayer);
	void onMoveMiss();
	void onMoveInvalid();
	void onMoveShipHit();
	void onMoveShipSink();
	void onFinish(FinishStatus status);
	void sleep(long ms);
	
}
