package fer.icr.battleship.game;

public class Move {
	public int row = 0;
	public int col = 0;

	public Move(int row, int col) {
		if (row >= 0 && row < Grid.SIZE && col >= 0 && col < Grid.SIZE) {
			this.row = row;
			this.col = col;
		}
	}

}
