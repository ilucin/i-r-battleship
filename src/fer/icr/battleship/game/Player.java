package fer.icr.battleship.game;

import java.util.ArrayList;

import fer.icr.battleship.utils.Randomizer;

public class Player {

	public enum Type {
		HUMAN, COMPUTER
	}

	public enum MoveResult {
		PREPARE, HIT, MISS, INVALID
	}

	private Type type;
	protected String name;
	protected Grid grid;
	protected int score;
	private MoveResult moveResult;
	private ArrayList<Ship> shipList;

	public Player(String name, Type type) {
		this.name = name;
		this.type = type;
		grid = new Grid();
		shipList = new ArrayList<Ship>();
		initShips();
	}

	public boolean isComputer() {
		return this.type == Type.COMPUTER;
	}

	public boolean isHuman() {
		return this.type == Type.HUMAN;
	}

	private void initShips() {
		for (int i = 0; i < 4; i++) {
			shipList.add(new Ship(1, "Čamac"));
		}
		for (int i = 0; i < 3; i++) {
			shipList.add(new Ship(2, "Putnički brod"));
		}
		for (int i = 0; i < 2; i++) {
			shipList.add(new Ship(3, "Trajekt"));
		}
		for (int i = 0; i < 1; i++) {
			shipList.add(new Ship(4, "Tanker"));
		}
	}

	public Grid getGrid() {
		return grid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return this.score;
	}

	public void incrementScore() {
		this.score++;
	}

	public String getLabel() {
		return this.name + " : " + this.score;
	}

	public boolean lastMoveIsHit() {
		return moveResult == MoveResult.HIT;
	}

	public boolean lastMoveIsMiss() {
		return moveResult == MoveResult.MISS;
	}

	public boolean lastMoveIsInvalid() {
		return moveResult == MoveResult.INVALID;
	}

	public void computerMove() {
		Move move;

		ArrayList<Move> expectedMoves = grid.getExpectedMoves();
		if (expectedMoves.size() > 0) {
			move = expectedMoves.get(0);
		} else {

			ArrayList<Move> possibleMoves = grid.getPossibleMoves();
			int index = Randomizer.GetInt(0, possibleMoves.size());
			move = possibleMoves.get(index);
		}

		// Prepare move
		makeMove(move.row, move.col);
		// Make move
		makeMove(move.row, move.col);
	}

	public void makeMove(int row, int col) {
		moveResult = grid.move(row, col);
		score += grid.getMoveScore();
	}

	public boolean placeShipsOnTheGrid() {
		final int limit = 10000;
		int counter = 0;

		while (!shipList.isEmpty() && counter++ < limit) {
			Ship ship = shipList.get(0);

			int row = Randomizer.GetInt(0, Grid.SIZE);
			int col = Randomizer.GetInt(0, Grid.SIZE);

			if (Randomizer.GetBoolean())
				ship.reverseOrientation();

			if (grid.placeShip(ship, row, col))
				shipList.remove(0);
		}

		if (counter >= limit)
			return false;
		else
			return true;

	}

}
