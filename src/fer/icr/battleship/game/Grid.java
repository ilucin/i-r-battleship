package fer.icr.battleship.game;

import java.util.ArrayList;

import fer.icr.battleship.game.Player.MoveResult;

public class Grid {

	public static final int SIZE = 10;

	private ArrayList<Ship> shipList;
	private Cell[][] cells;
	private boolean enabled, shipsVisible;
	private Cell preparedCell = null;
	private int moveScore = 0;
	private int shipsSinked = 0;

	public Grid() {
		cells = new Cell[SIZE][SIZE];
		shipList = new ArrayList<Ship>();

		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				cells[i][j] = new Cell(i, j);
			}
		}
	}

	public MoveResult move(int row, int col) {

		MoveResult res = cells[row][col].bomb();

		if (res == MoveResult.HIT) {
			processHit(cells[row][col]);
		} else
			moveScore = 0;

		if (preparedCell != null && !preparedCell.checkPosition(row, col))
			preparedCell.unprepare();

		if (res == MoveResult.PREPARE) {
			preparedCell = cells[row][col];
		}

		return res;
	}

	private void processHit(Cell cell) {
		moveScore = cell.getMoveScore();

		if (cell.getShip().isSinked()) {
			shipsSinked++;
			processSink(cell);
		}

	}

	private void processSink(Cell cell) {
		Ship ship = cell.getShip();
		int row = cell.getRow();
		int col = cell.getCol();

		if (ship.isHorizontal()) {

			while (col > 0 && cells[row][col - 1].hasShip())
				col--;

			for (int i = row - 1; i <= row + 1; i++) {
				for (int j = col - 1; j < col + ship.getLen() + 1; j++) {
					if (i < 0 || i >= SIZE || j < 0 || j >= SIZE)
						continue;
					else {
						if (!cells[i][j].hasShip()) {
							cells[i][j].setMiss();
						}
					}
				}
			}
		} else if (ship.isVertical()) {

			while (row > 0 && cells[row - 1][col].hasShip())
				row--;

			for (int i = row - 1; i < row + ship.getLen() + 1; i++) {
				for (int j = col - 1; j <= col + 1; j++) {
					if (i < 0 || i >= SIZE || j < 0 || j >= SIZE)
						continue;
					else {
						if (!cells[i][j].hasShip()) {
							cells[i][j].setMiss();
						}
					}
				}
			}
		}

	}

	public boolean allShipsSinked() {
		return shipsSinked == shipList.size();
	}

	public Cell getCell(int row, int col) {
		if (row < 0 || row >= SIZE || col < 0 || col >= SIZE)
			return null;
		return cells[row][col];
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setShipsVisible(boolean shipsVisible) {
		this.shipsVisible = shipsVisible;
	}

	public int getMoveScore() {
		return moveScore;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public boolean shipsVisible() {
		return this.shipsVisible;
	}

	public ArrayList<Move> getExpectedMoves() {

		ArrayList<Move> moves = new ArrayList<Move>();
		for (int i = 0; i < Grid.SIZE; i++) {
			for (int j = 0; j < Grid.SIZE; j++) {

				Cell c = cells[i][j];

				if (c.isStateEmpty() || c.isStateShip()) {

					Move m = new Move(i, j);
					Cell cTop, cBot, cLef, cRig, cTopTop, cBotBot, cLefLef, cRigRig;
					cTop = i > 0 ? cells[i - 1][j] : null;
					cTopTop = i > 1 ? cells[i - 2][j] : null;
					cBot = i < Grid.SIZE - 1 ? cells[i + 1][j] : null;
					cBotBot = i < Grid.SIZE - 2 ? cells[i + 2][j] : null;
					cLef = j > 0 ? cells[i][j - 1] : null;
					cLefLef = j > 1 ? cells[i][j - 2] : null;
					cRig = j < Grid.SIZE - 1 ? cells[i][j + 1] : null;
					cRigRig = j < Grid.SIZE - 2 ? cells[i][j + 2] : null;

					if (c.isStateEmpty() || c.isStateShip()) {

						if (cTop != null && cTop.isStateHit()) {
							if (cTopTop != null && cTopTop.isStateHit()) {
								moves.clear();
								moves.add(m);
								return moves;
							} else
								moves.add(m);
						}

						if (cBot != null && cBot.isStateHit()) {
							if (cBotBot != null && cBotBot.isStateHit()) {
								moves.clear();
								moves.add(m);
								return moves;
							} else
								moves.add(m);
						}

						if (cRig != null && cRig.isStateHit()) {
							if (cRigRig != null && cRigRig.isStateHit()) {
								moves.clear();
								moves.add(m);
								return moves;
							} else
								moves.add(m);
						}
						if (cLef != null && cLef.isStateHit()) {
							if (cLefLef != null && cLefLef.isStateHit()) {
								moves.clear();
								moves.add(m);
								return moves;
							} else
								moves.add(m);
						}

					}
				}
			}
		}
		return moves;
	}

	public ArrayList<Move> getPossibleMoves() {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (int i = 0; i < Grid.SIZE; i++) {
			for (int j = 0; j < Grid.SIZE; j++) {
				if (cells[i][j].isStateEmpty() || cells[i][j].isStateShip()) {
					moves.add(new Move(i,j));
				}
			}
		}
		return moves;
	}

	public boolean checkValidShipPlacement(Ship ship, int row, int col) {
		if (row < 0 || row >= SIZE || col < 0 || col >= SIZE)
			return false;

		if (ship.isHorizontal()) {
			if (col > (SIZE - ship.getLen()))
				return false;

			for (int i = row - 1; i <= row + 1; i++) {
				for (int j = col - 1; j <= col + ship.getLen() + 1; j++) {
					if (i < 0 || i >= SIZE || j < 0 || j >= SIZE)
						continue;
					else {
						if (cells[i][j].hasShip())
							return false;
					}
				}
			}

		} else if (ship.isVertical()) {
			if (row > (SIZE - ship.getLen()))
				return false;

			for (int i = row - 1; i <= row + ship.getLen() + 1; i++) {
				for (int j = col - 1; j <= col + 1; j++) {
					if (i < 0 || i >= SIZE || j < 0 || j >= SIZE)
						continue;
					else {
						if (cells[i][j].hasShip())
							return false;
					}
				}
			}
		}

		return true;
	}

	public boolean placeShip(Ship ship, int row, int col) {
		if (checkValidShipPlacement(ship, row, col)) {
			for (int i = 0; i < ship.getLen(); i++) {
				if (ship.isHorizontal()) {
					cells[row][col + i].setShip(ship);
				} else if (ship.isVertical()) {
					cells[row + i][col].setShip(ship);
				}
			}
			shipList.add(ship);
			return true;
		} else
			return false;
	}

}
