package fer.icr.battleship.game;

import fer.icr.battleship.game.Player.MoveResult;

public class Cell {

	public enum State {
		EMPTY, SHIP, MISS, HIT
	}

	private State state;
	private Ship ship;
	private int row,col;
	private boolean prepared;

	public Cell(int row, int col) {
		this.row = row;
		this.col = col;
		this.state = State.EMPTY;
		this.prepared = false;
	}
	
	public boolean hasShip() {
		return ship != null;
	}
	
	public boolean isStateEmpty() {
		return state == State.EMPTY;
	}

	public boolean isStateShip() {
		return state == State.SHIP;
	}

	public boolean isStateMiss() {
		return state == State.MISS;
	}

	public boolean isStateHit() {
		return state == State.HIT;
	}

	public boolean isPrepared() {
		return prepared;
	}
	
	public void setMiss() {
		state = State.MISS;
	}

	public MoveResult bomb() {
		if (prepared) {
			prepared = false;

			if (state == State.EMPTY) {
				state = State.MISS;
				return MoveResult.MISS;
			}
			else if (state == State.SHIP) {
				state = State.HIT;
				ship.hit();
				return MoveResult.HIT;
			}
			else return MoveResult.INVALID;
		} else {
			prepared = true;
			return MoveResult.PREPARE;
		}
	}
	
	public void unprepare() {
		prepared = false;
	}
	
	public boolean checkPosition(int row, int col) {
		return row == this.row && col == this.col;
	}
	
	public void setShip(Ship ship) {
		this.ship = ship;
		this.state = State.SHIP;
	}
	
	public Ship getShip() {
		return this.ship;
	}
	
	public int getMoveScore() {
		if (this.state == State.HIT && ship.isSinked()) return ship.getSinkScore();
		else return 0;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}

}
