package fer.icr.battleship.game;

public class Ship {

	private int len, lenSinked;
	private String name;
	private Orientation orientation;

	public enum Orientation {
		VERTICAL, HORIZONTAL
	}

	public Ship(int len, String name) {
		this.len = len;
		this.lenSinked = 0;
		this.name = name;
		orientation = Orientation.HORIZONTAL;
	}
	
	public void hit() {
		if (lenSinked < len)
			lenSinked++;
	}
	
	public boolean isSinked() {
		return lenSinked >= len;
	}

	public int getLen() {
		return len;
	}

	public String getName() {
		return name;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public int getSinkScore() {
		return 20 + (12 / this.len);
	}

	public boolean isVertical() {
		return this.orientation == Orientation.VERTICAL;
	}

	public boolean isHorizontal() {
		return this.orientation == Orientation.HORIZONTAL;
	}

	public void reverseOrientation() {
		orientation = orientation == Orientation.HORIZONTAL ? Orientation.VERTICAL
				: Orientation.HORIZONTAL;
	}
}
