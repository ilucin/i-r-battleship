package fer.icr.battleship.game;

public class Game {

	public enum Mode {
		VS_HUMAN, VS_COMPUTER
	}

	public enum FinishStatus {
		WIN, LOSE, CANCEL
	}

	private IGameContext ctx;
	private Player player1, player2, activePlayer, inactivePlayer;
	private Mode mode;

	public Game(IGameContext ctx, Player player1, Player player2) {

		this.ctx = ctx;
		this.player1 = player1;
		this.player2 = player2;
		this.mode = player2.isComputer() ? Mode.VS_COMPUTER : Mode.VS_HUMAN;
	}

	public void start() {
		activePlayer = player2;
		inactivePlayer = player1;

		switchPlayer();
		updateLabels();

	}

	public void humanMove(int row, int col) {
		activePlayer.makeMove(row, col);

		if (activePlayer.lastMoveIsHit())
			processHit();
		else if (activePlayer.lastMoveIsMiss())
			processMiss();
	}

	public void computerMove() {
		do {
			activePlayer.computerMove();

			if (activePlayer.lastMoveIsHit())
				processHit();

		} while (activePlayer.lastMoveIsHit());

		switchPlayer();
		updateLabels();
	}

	private void processHit() {
		ctx.onMoveShipHit();
		updateLabels();

		if (activePlayer.getGrid().allShipsSinked()) {
			finish();
		}
	}

	private void processMiss() {
		switchPlayer();
		updateLabels();

		if (activePlayer.isComputer()) {
			computerMove();
		}
	}

	private void switchPlayer() {

		activePlayer.getGrid().setEnabled(false);
		inactivePlayer.getGrid().setEnabled(false);

		if (gameModeVsHuman()) {
			activePlayer.getGrid().setShipsVisible(false);
			inactivePlayer.getGrid().setShipsVisible(false);
		}
		
		activePlayer = activePlayer == player1 ? player2 : player1;
		inactivePlayer = inactivePlayer == player1 ? player2 : player1;

		ctx.sleep(200);

		if (gameModeVsHuman())
			ctx.showPlayerSwitchDialog(activePlayer);
		else {
			if (activePlayer.isHuman()) {
				activePlayer.getGrid().setEnabled(true);
				inactivePlayer.getGrid().setShipsVisible(true);
			}
		}
	}

	public void continueAfterSwitchDialog() {
		inactivePlayer.getGrid().setEnabled(false);
		inactivePlayer.getGrid().setShipsVisible(true);
		activePlayer.getGrid().setEnabled(true);
		activePlayer.getGrid().setShipsVisible(false);
	}

	public void finish() {
		FinishStatus status;

		if (activePlayer.getGrid().allShipsSinked()) {
			if (activePlayer == player1)
				status = FinishStatus.WIN;
			else
				status = FinishStatus.LOSE;
		} else
			status = FinishStatus.CANCEL;

		ctx.onFinish(status);
	}

	private void updateLabels() {
		ctx.updateLabels(activePlayer, inactivePlayer);
	}

	public boolean gameModeVsComputer() {
		return mode == Mode.VS_COMPUTER;
	}

	public boolean gameModeVsHuman() {
		return mode == Mode.VS_HUMAN;
	}

}
