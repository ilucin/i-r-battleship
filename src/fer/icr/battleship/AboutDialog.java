package fer.icr.battleship;

import android.app.Dialog;
import android.content.Context;

import android.os.Bundle;


public class AboutDialog extends Dialog {

	public AboutDialog(Context context) {
		super(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.about);
	}

}
