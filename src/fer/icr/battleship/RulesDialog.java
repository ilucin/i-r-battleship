package fer.icr.battleship;

import android.app.Dialog;
import android.content.Context;

import android.os.Bundle;


public class RulesDialog extends Dialog {

	public RulesDialog(Context context) {
		super(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.rules);
	}

}
