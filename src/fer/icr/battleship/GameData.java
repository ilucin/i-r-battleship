package fer.icr.battleship;

import android.content.SharedPreferences;

public class GameData {

	private static GameData instance;

	public static final String PREFS_FILENAME = "BattleshipPrefs";
	
	public String player1, player2;
	public boolean vibration, sound, closeConfirm;
	public int wins, loses, totalGames, canceledGames, totalScore, avgScore;
	
	private SharedPreferences prefs;
	
	public static GameData instance() {
		if (instance == null)
			instance = new GameData();
		return instance;
	}
	
	private GameData() {
		player1 = "Player 1";
		player2 = "Player 2";
		vibration = true;
		sound = false;
		closeConfirm = true;
		wins = 0;
		loses = 0;
		totalGames = 0;
		canceledGames = 0;
		totalScore = 0;
		avgScore = 0;
	}

	public void setSharedPreferences(SharedPreferences prefs) {
		this.prefs = prefs;
		refresh();
	}
	
	public void refresh() {
		initData();
	}
	
	private void initData() {
		player1 = prefs.getString("player1", player1);
		player2 = prefs.getString("player2", player2);
		vibration = prefs.getBoolean("vibration", vibration);
		sound = prefs.getBoolean("sound", sound);
		closeConfirm = prefs.getBoolean("closeConfirm", closeConfirm);
		wins = prefs.getInt("wins", wins);
		loses = prefs.getInt("loses", loses);
		totalGames = prefs.getInt("totalGames", totalGames);
		canceledGames = prefs.getInt("canceledGames", canceledGames);
		totalScore = prefs.getInt("totalScore", totalScore);
		avgScore = prefs.getInt("avgScore",avgScore);
	}
	
	public void saveChanges() {
		SharedPreferences.Editor prefsEditor = prefs.edit();

		prefsEditor.putInt("totalGames", totalGames);
		prefsEditor.putInt("wins", wins);
		prefsEditor.putInt("loses", loses);
		prefsEditor.putInt("totalScore", totalScore);
		prefsEditor.putInt("avgScore", avgScore);
		prefsEditor.putInt("canceledGames", canceledGames);
		
		prefsEditor.putString("player1", player1);
		prefsEditor.putString("player2", player2);
		prefsEditor.putBoolean("vibration", vibration);
		prefsEditor.putBoolean("sound", sound);
		prefsEditor.putBoolean("closeConfirm", closeConfirm);

		prefsEditor.commit();
	}
	
	public void resetStatistics() {
		totalGames = 0;
		wins = 0;
		loses = 0;
		totalScore = 0;
		avgScore = 0;
		canceledGames = 0;
		
		if (prefs != null)
			saveChanges();
	}
	
}
