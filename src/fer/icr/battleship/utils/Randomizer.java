package fer.icr.battleship.utils;

import java.util.Random;

public class Randomizer {

	public static Random rand = new Random();
	
	public static int GetInt(int min, int max) {
		return rand.nextInt(max-min)+min;
	}

	public static boolean GetBoolean() {
		return rand.nextBoolean();
	}
}
