package fer.icr.battleship;

import fer.icr.battleship.game.Cell;
import fer.icr.battleship.game.Game;
import fer.icr.battleship.game.Grid;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class GridSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback, OnTouchListener {

	// private static final String TAG = "GridSurfaceView";
	private static final int BORDER_WIDTH = 2;
	private static final int TABLE_RADIUS = 5;

	private MainThread thread;
	private Grid grid;
	private Game game;
	private SurfaceHolder surfaceHolder;
	private Paint linePaint, touchPaint, contentPaint,
			backgroundPaint, surfacePaint, hitPaint, missPaint, shipPaint,
			preparePaint;
	private int gridH, gridW, cellH, cellW, touchRow, touchCol, surfW, surfH;
	private Rect[][] cells;
	private boolean touch;

	public GridSurfaceView(Context context) {
		super(context);
	}

	public GridSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GridSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void init(Game game, Grid grid) {
		this.surfaceHolder = getHolder();
		this.surfaceHolder.addCallback(this);
		this.linePaint = new Paint();
		this.touchPaint = new Paint();
		this.contentPaint = new Paint();
		this.backgroundPaint = new Paint();
		this.surfacePaint = new Paint();
		this.hitPaint = new Paint();
		this.missPaint = new Paint();
		this.shipPaint = new Paint();
		this.preparePaint = new Paint();
		this.cells = new Rect[Grid.SIZE][Grid.SIZE];
		this.thread = new MainThread(getHolder());
		this.grid = grid;
		this.game = game;

		linePaint.setStyle(Style.STROKE);
		// linePaint.setShader(new LinearGradient(0, 0, gridH, gridW,0xFF000000,
		// 0xFF343434, TileMode.MIRROR));
		linePaint.setStrokeWidth(BORDER_WIDTH);
		linePaint.setAlpha(0xCC);

		touchPaint.setStyle(Style.FILL_AND_STROKE);
		touchPaint.setStrokeWidth(BORDER_WIDTH);
		touchPaint.setColor(0x66000000);

		backgroundPaint.setColor(0xFFFFFF00);
		backgroundPaint.setPathEffect(new CornerPathEffect(TABLE_RADIUS));

		surfacePaint.setColor(0xFF33B5E5);

		preparePaint.setColor(0x33000000);
		hitPaint.setColor(0xFFFF0000);
		missPaint.setColor(0xFF00FF00);
		shipPaint.setColor(0xFF0000FF);

		contentPaint.setAlpha(0xDF);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		Rect rect = holder.getSurfaceFrame();

		this.touch = false;

		this.surfW = rect.width();
		this.surfH = rect.height();

		this.cellW = (int) Math
				.floor((surfW - (BORDER_WIDTH * (Grid.SIZE + 1))) / Grid.SIZE);
		this.cellH = (int) Math
				.floor((surfH - (BORDER_WIDTH * (Grid.SIZE + 1))) / Grid.SIZE);
		
		//if (cellW < cellH) cellH = cellW;
		//else cellW = cellH;

		this.gridH = Grid.SIZE * cellH + (Grid.SIZE) * BORDER_WIDTH;
		this.gridW = Grid.SIZE * cellW + (Grid.SIZE) * BORDER_WIDTH;

		for (int i = 0; i < Grid.SIZE; i++) {
			for (int j = 0; j < Grid.SIZE; j++) {
				int left = ((j + 1) * BORDER_WIDTH) + (cellW * j);
				int top = ((i + 1) * BORDER_WIDTH) + (cellH * i);
				cells[i][j] = new Rect(left, top, left + cellW - BORDER_WIDTH,
						top + cellH - BORDER_WIDTH);
			}
		}

		setOnTouchListener(this);

		startMainThread();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		stopMainThread();
	}

	public void stopMainThread() {
		if (thread != null && thread.isRunning()) {
			boolean retry = true;
			thread.setRunning(false);
			while (retry) {
				try {
					thread.join();
					retry = false;
				} catch (InterruptedException e) {

				}
			}
		}
	}

	public void startMainThread() {
		if (thread != null && !thread.isRunning()) {
			thread.setRunning(true);
			thread.start();
		}
	}

	class MainThread extends Thread {

		private SurfaceHolder surfaceHolder;
		private boolean runFlag = false;
		private long sleepTime = 100; // ms

		public MainThread(SurfaceHolder surfaceHolder) {
			this.surfaceHolder = surfaceHolder;
		}

		public void setRunning(boolean run) {
			this.runFlag = run;
		}

		public boolean isRunning() {
			return this.isAlive() || this.runFlag;
		}

		@Override
		public void run() {
			Canvas c;

			while (this.runFlag) {

				c = null;
				try {
					c = this.surfaceHolder.lockCanvas(null);
					synchronized (this.surfaceHolder) {
						doDraw(c);
					}
				} finally {
					if (c != null) {
						this.surfaceHolder.unlockCanvasAndPost(c);
					}
				}

				try {
					sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		if (this.grid.isEnabled()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {

				touchCol = (int) Math.floor((event.getX() / gridW) * Grid.SIZE);
				touchRow = (int) Math.floor((event.getY() / gridH) * Grid.SIZE);

				if (touchCol >= 0 && touchCol < Grid.SIZE && touchRow >= 0
						&& touchRow < Grid.SIZE)
					touch = true;

			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				this.touch = false;

				touchCol = (int) Math.floor((event.getX() / gridW) * Grid.SIZE);
				touchRow = (int) Math.floor((event.getY() / gridH) * Grid.SIZE);

				if (touchCol >= 0 && touchCol < Grid.SIZE && touchRow >= 0
						&& touchRow < Grid.SIZE)
					game.humanMove(touchRow, touchCol);
			}

			return true;
		} else
			return false;
	}

	public void doDraw(Canvas canvas) {

		if (canvas != null) {
			drawSurfaceBackground(canvas);

			drawTable(canvas);

			drawContent(canvas);

			if (this.touch) {
				drawTouch(canvas);
			}
		}

	}

	private void drawSurfaceBackground(Canvas canvas) {
		canvas.drawRect(0, 0, surfW, surfH, surfacePaint);
	}

	private void drawTable(Canvas canvas) {

		// Draw table background
		Rect tableRect = new Rect(0, 0, gridW, gridH);
		canvas.drawRect(tableRect, backgroundPaint);

		// Draw vertical grid lines
		for (int i = 1; i < Grid.SIZE; i++) {
			float x = i * (cellW + BORDER_WIDTH);
			float startY = 0;
			float stopY = gridH;
			canvas.drawLine(x, startY, x, stopY, linePaint);
		}

		// Draw horizontal grid lines
		for (int i = 1; i < Grid.SIZE; i++) {
			float y = i * (cellH + BORDER_WIDTH);
			float startX = 0;
			float stopX = gridW;
			canvas.drawLine(startX, y, stopX, y, linePaint);
		}

		// Draw outer table rectangle with border radius
		linePaint.setPathEffect(new CornerPathEffect(TABLE_RADIUS));
		canvas.drawRect(0, 0, gridW, gridH, linePaint);
		linePaint.setPathEffect(null);

	}

	private void drawContent(Canvas canvas) {

		for (int i = 0; i < Grid.SIZE; i++) {
			for (int j = 0; j < Grid.SIZE; j++) {
				Cell c = grid.getCell(i, j);

				if (c.isStateHit()) {
					canvas.drawRect(cells[i][j], hitPaint);
				} else if (c.isStateMiss()) {
					canvas.drawRect(cells[i][j], missPaint);
				} else if (c.isStateShip() && grid.shipsVisible()) {
					canvas.drawRect(cells[i][j], shipPaint);
				}

				if (c.isPrepared()) {
					canvas.drawRect(cells[i][j], preparePaint);
				}

			}
		}
	}

	private void drawTouch(Canvas canvas) {
		canvas.drawRect(cells[touchRow][touchCol], touchPaint);
	}

}
