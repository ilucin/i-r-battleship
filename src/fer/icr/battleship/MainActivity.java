package fer.icr.battleship;

import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	SectionsPagerAdapter mSectionsPagerAdapter;
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		GameData.instance().setSharedPreferences(
				getSharedPreferences(GameData.PREFS_FILENAME, 0));

		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_main_about:
			showAboutDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void showAboutDialog() {
		AboutDialog about = new AboutDialog(this);
		about.setTitle("About IČR Battleship");
		about.show();
	}

	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return new MainMenuFragment();
			case 1:
				return new StatisticsFragment();
			case 2:
				return new SettingsFragment();
			default:
				return new Fragment();
			}
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	public static class MainMenuFragment extends Fragment implements
			OnClickListener {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_main_menu, container,
					false);
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			Button btnExit = (Button) getActivity().findViewById(R.id.btnExit);
			Button btnVsHuman = (Button) getActivity().findViewById(
					R.id.btnPlayVsHuman);
			Button btnVsComputer = (Button) getActivity().findViewById(
					R.id.btnPlayVsComp);
			Button btnRules = (Button) getActivity()
					.findViewById(R.id.btnRules);

			btnVsComputer.setActivated(false);

			btnExit.setOnClickListener(this);
			btnVsHuman.setOnClickListener(this);
			btnVsComputer.setOnClickListener(this);
			btnRules.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btnExit: {
				getActivity().finish();
				break;
			}
			case R.id.btnPlayVsHuman: {
				startGameVsHuman();
				break;
			}
			case R.id.btnPlayVsComp: {
				startGameVsComputer();
				break;
			}
			case R.id.btnRules: {
				showRulesDialog();
				break;
			}
			}
		}
		
		private void startGameVsHuman() {
			Intent it = new Intent(getActivity(), GameActivity.class);
			it.putExtra("typeVsComputer", false);
			getActivity().startActivity(it);
		}
		
		private void startGameVsComputer() {
			Intent it = new Intent(getActivity(), GameActivity.class);
			it.putExtra("typeVsComputer", true);
			getActivity().startActivity(it);
		}
		
		private void showRulesDialog() {
			RulesDialog dialog = new RulesDialog(getActivity());
			dialog.setTitle("How to play Battleship game?");
			dialog.show();
		}

	}

	public static class StatisticsFragment extends Fragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_statistics, container,
					false);
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			showStatistics();

			Button btnReset = (Button) getActivity().findViewById(R.id.btnStatsReset);

			btnReset.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					GameData.instance().resetStatistics();
					showStatistics();
				}
			});
		}

		@Override
		public void onResume() {
			showStatistics();
			super.onResume();
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			showStatistics();
			super.onActivityResult(requestCode, resultCode, data);
		}

		public void showStatistics() {

			TextView txtAvgScore = (TextView) getActivity().findViewById(
					R.id.txtStatsAvgScoreValue);
			TextView txtCanceled = (TextView) getActivity().findViewById(
					R.id.txtStatsGamesCanceledValue);
			TextView txtLooses = (TextView) getActivity().findViewById(
					R.id.txtStatsLoosesValue);
			TextView txtTotalGames = (TextView) getActivity().findViewById(
					R.id.txtStatsTotalGamesValue);
			TextView txtTotalScore = (TextView) getActivity().findViewById(
					R.id.txtStatsTotalScoreValue);
			TextView txtWins = (TextView) getActivity().findViewById(
					R.id.txtStatsWinsValue);

			txtWins.setText(String.valueOf(GameData.instance().wins));
			txtLooses.setText(String.valueOf(GameData.instance().loses));
			txtTotalGames
					.setText(String.valueOf(GameData.instance().totalGames));
			txtCanceled
					.setText(String.valueOf(GameData.instance().canceledGames));
			txtAvgScore.setText(String.valueOf(GameData.instance().avgScore));
			txtTotalScore
					.setText(String.valueOf(GameData.instance().totalScore));

		}
	}

	public static class SettingsFragment extends Fragment {

		private EditText txtPlayerOne, txtPlayerTwo;
		private CheckBox chkSound, chkVibration, chkCloseConfirm;
		private Button btnSave;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_settings, container,
					false);
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			txtPlayerOne = (EditText) getActivity().findViewById(
					R.id.txtSettingsPlayer1);
			txtPlayerTwo = (EditText) getActivity().findViewById(
					R.id.txtSettingsPlayer2);
			chkSound = (CheckBox) getActivity().findViewById(
					R.id.chkSettingsSound);
			chkVibration = (CheckBox) getActivity().findViewById(
					R.id.chkSettingsVibration);
			chkCloseConfirm = (CheckBox) getActivity().findViewById(
					R.id.chkSettingsExitConfirm);
			btnSave = (Button) getActivity().findViewById(R.id.btnSettingsSave);

			btnSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					saveSettings();
				}
			});

			showSettings();
		}

		public void showSettings() {
			txtPlayerOne.setText(GameData.instance().player1);
			txtPlayerTwo.setText(GameData.instance().player2);
			chkVibration.setChecked(GameData.instance().vibration);
			chkSound.setChecked(GameData.instance().sound);
			chkCloseConfirm.setChecked(GameData.instance().closeConfirm);
		}

		public void saveSettings() {
			GameData data = GameData.instance();

			data.player1 = txtPlayerOne.getText().toString();
			data.player2 = txtPlayerTwo.getText().toString();
			data.vibration = chkVibration.isChecked();
			data.sound = chkSound.isChecked();
			data.closeConfirm = chkCloseConfirm.isChecked();

			data.saveChanges();
			Toast.makeText(getActivity(), "Settings has been saved!",
					Toast.LENGTH_SHORT).show();
		}

	}

}
